<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;

class Problem extends Model implements AuthenticatableContract
{

  use Authenticatable;

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'tbl_problem';
  
  public $timestamps = false;

  protected $fillable = [
    'nama_perusahaan', 
    'pic_name', 
    'id_problem', 
    'id_ticket', 
    'detail_masalah'
  ];

}