<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;

class MstProblem extends Model implements AuthenticatableContract
{

  use Authenticatable;

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'mst_problem_mgmt';

}