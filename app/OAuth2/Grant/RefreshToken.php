<?php namespace App\OAuth2\Grant;

use League\OAuth2\Server\Grant\RefreshTokenGrant;

/**
 * Referesh token grant
 */
class RefreshToken extends RefreshTokenGrant
{
  /**
   * {@inheritdoc}
   */
  public function completeFlow()
  {
    parent::completeFlow();
    
    $accessToken = $this->server->getTokenType()->getParam('access_token');
    $accessToken = $this->server->getAccessTokenStorage()->get($accessToken);

    $this->server->getTokenType()->setParam('expires', (int) $accessToken->getExpireTime());

    return $this->server->getTokenType()->generateResponse();
  }
}
