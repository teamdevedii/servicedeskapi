<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Controllers\OAuth2;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse as Response;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Hashing\BcryptHasher;

use App\Http\Validators\UserValidator;
use App\Models\User;
use App\Models\MstProblem;

class MstProblemController extends Controller {

  use OAuth2;

  public function show($id){
  	// $data = MstProblem::select('code')->where('code',$id)->count();
  	$data = MstProblem::select('code')->where('level',$id)->count();

  	if($data){
  		return json_encode(array('layanan'=>MstProblem::select('code','problem_desc as deskripsi')->where('level',$id)->get()));
  	}else{
  		return json_encode(array('layanan'=>array('code'=>'erorr 502', 'deskripsi'=>'Data Tidak Tersedia')));
  	}
  }
}