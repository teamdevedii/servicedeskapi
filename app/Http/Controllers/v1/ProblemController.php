<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Controllers\OAuth2;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse as Response;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Hashing\BcryptHasher;

use App\Http\Validators\UserValidator;
use App\Models\User;
use App\Models\MstProblem;
use App\Models\Problem;

class ProblemController extends Controller {

  use OAuth2;

  public function store(Request $request){

  	$insert = Problem::create(
  			array(
  					'id_ticket' => '',
  					'nama_perusahaan' => $request->nama_perusahaan,
  					'contact_person' => $request->contact_person,
  					'id_problem' => $request->code,
  					'detail_masalah' => $request->detail_masalah
  				)
  		);

	if($insert){
  		return json_encode(array('putTicket'=>array('id_ticket'=>$insert->id)));
	}else{
  		return json_encode(array('putTicket'=>array('id_ticket'=>'')));
	}
  }

  public function updateTicket(Request $request)
  {
  	$update = Problem::where('id_ticket',$request->id_ticket)->update(
  			array(
  					'detail_masalah' => $request->detail_masalah
  				)
  		);

  	if($update){
  		return json_encode(array('updTicket' => array('id_ticket'=>$request->id_ticket, 'message' => array('error' => '200', 'success' => 'Data OK'))));
  	}else{
  		return json_encode(array('updTicket'=>array('id_ticket'=>$request->id_ticket, 'message' => array('error' => '502', 'success' => 'Data Tidak Tersedia'))));
  	}
  }

  public function getStatus($id)
  {
  	$data = Problem::leftjoin('mst_user','tbl_problem.solver_id','=','mst_user.nik')
  			->leftjoin('mst_status','tbl_problem.status','=','mst_status.id')
  			->select('id_ticket','nama_perusahaan','contact_person','mst_user.nama as solver_name','date_solved','time_created','mst_status.status_name as status','detail_masalah')
  			->where('id_ticket',$id)
  			->get();

  	if($data){
  		return json_encode(array('getTicket'=>array('data'=>$data, 'message' => array('error' => '200', 'success' => 'Data OK'))));
  	}else{
  		return json_encode(array('getTicket'=>array('data'=> array('data' => 'Data Tidak Tersedia'), 'message' => array('error' => '502', 'success' => 'Data tidak tersedia'))));
  	}
  }

  public function getDetailTicketBulk(Request $request)
  {
    $tickets = explode(',',$request->tickets);

    // $ticketIn = "";
    // foreach ($tickets as $ticket) {
    //   $ticketIn = $ticketIn.$ticket."==";
    // }

    $data = Problem::leftjoin('mst_user','tbl_problem.solver_id','=','mst_user.nik')
        ->leftjoin('mst_status','tbl_problem.status','=','mst_status.id')
        ->select('id_ticket','nama_perusahaan','contact_person','mst_user.nama as solver_name','date_solved','time_created','mst_status.status_name as status','detail_masalah')
        ->whereRaw('id_ticket IN ('.$request->tickets.')')
        ->orderby('id_ticket')
        ->get();

    if($data){
      return json_encode(array('getTicket'=>array('data'=>$data, 'message' => array('error' => '200', 'success' => 'Data OK'))));
    }else{
      return json_encode(array('getTicket'=>array('data'=> array('data' => 'Data Tidak Tersedia'), 'message' => array('error' => '502', 'success' => 'Data tidak Tersedia'))));
    }
  }
}
