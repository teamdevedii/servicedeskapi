SET NAMES utf8;
SET time_zone = '+00:00';
SET FOREIGN_KEY_CHECKS = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';


CREATE TABLE `oauth_access_token` (
  `access_token` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` INT(10) UNSIGNED NOT NULL,
  `expire_time` INT(11) NOT NULL,
  PRIMARY KEY (`access_token`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `oauth_access_token_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `oauth_session` (`id`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `oauth_access_token_scope` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `access_token` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `scope` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `access_token` (`access_token`),
  KEY `scope` (`scope`),
  CONSTRAINT `oauth_access_token_scope_ibfk_1` FOREIGN KEY (`access_token`) REFERENCES `oauth_access_token` (`access_token`) ON DELETE CASCADE,
  CONSTRAINT `oauth_access_token_scope_ibfk_2` FOREIGN KEY (`scope`) REFERENCES `oauth_scope` (`id`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `oauth_auth_code` (
  `auth_code` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `session_id` INT(10) UNSIGNED NOT NULL,
  `expire_time` INT(11) NOT NULL,
  `client_redirect_uri` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`auth_code`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `oauth_auth_code_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `oauth_session` (`id`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `oauth_auth_code_scope` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `auth_code` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `scope` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_code` (`auth_code`),
  KEY `scope` (`scope`),
  CONSTRAINT `oauth_auth_code_scope_ibfk_1` FOREIGN KEY (`auth_code`) REFERENCES `oauth_auth_code` (`auth_code`) ON DELETE CASCADE,
  CONSTRAINT `oauth_auth_code_scope_ibfk_2` FOREIGN KEY (`scope`) REFERENCES `oauth_scope` (`id`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `oauth_client` (
  `id` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `secret` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `request_limit` INT(10) NOT NULL DEFAULT '5000',
  `current_total_request` INT(10) NOT NULL DEFAULT '0',
  `request_limit_until` TIMESTAMP NULL DEFAULT NULL,
  `last_request_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `oauth_client` (`id`, `secret`, `name`, `request_limit`, `current_total_request`, `request_limit_until`, `last_request_at`) VALUES
('q86tn9wkczKtFdD0',  'KuYoLBpOplXBy8EWy3uM6ev0wW4ldbY2', 'api',  500000, 0,  NULL, NULL);

CREATE TABLE `oauth_client_redirect_uri` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `redirect_uri` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `oauth_client_redirect_uri` (`id`, `client_id`, `redirect_uri`) VALUES
(1, 'q86tn9wkczKtFdD0', 'http://192.168.5.80/ServiceDeskMicroAPI/public/');

CREATE TABLE `oauth_mac_key` (
  `key` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `access_token` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`key`),
  KEY `access_token` (`access_token`),
  CONSTRAINT `oauth_mac_key_ibfk_1` FOREIGN KEY (`access_token`) REFERENCES `oauth_access_token` (`access_token`) ON DELETE NO ACTION
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `oauth_refresh_token` (
  `refresh_token` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `expire_time` INT(11) NOT NULL,
  `access_token` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`refresh_token`),
  KEY `access_token` (`access_token`),
  CONSTRAINT `oauth_refresh_token_ibfk_1` FOREIGN KEY (`access_token`) REFERENCES `oauth_access_token` (`access_token`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `oauth_scope` (
  `id` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `oauth_scope` (`id`, `description`) VALUES
('basic', 'Basic details about your account'),
('email', 'Your email address'),
('photo', 'Your photo');

CREATE TABLE `oauth_session` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `owner_type` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `owner_id` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `client_redirect_uri` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`),
  CONSTRAINT `oauth_session_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `oauth_client` (`id`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `oauth_session_scope` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `session_id` INT(10) UNSIGNED NOT NULL,
  `scope` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `scope` (`scope`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `oauth_session_scope_ibfk_1` FOREIGN KEY (`scope`) REFERENCES `oauth_scope` (`id`) ON DELETE CASCADE,
  CONSTRAINT `oauth_session_scope_ibfk_2` FOREIGN KEY (`session_id`) REFERENCES `oauth_session` (`id`) ON DELETE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `users` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
  `activated` TINYINT(1) NOT NULL DEFAULT '0',
  `activation_code` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` TIMESTAMP NULL DEFAULT NULL,
  `last_login` TIMESTAMP NULL DEFAULT NULL,
  `remember_token` VARCHAR(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` VARCHAR(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_activation_code_index` (`activation_code`(191)),
  KEY `users_reset_password_code_index` (`remember_token`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `users` (`id`, `email`, `password`, `activated`, `activation_code`, `activated_at`, `last_login`, `remember_token`, `first_name`, `last_name`, `created_at`, `updated_at`) VALUES
(1, 'apiservicedesk@edi-indonesia.co.id',  '$2y$10$Ccma/WrwIT70kHdzj6ApeOD46plrHkrlbzERuJYqFsVxcAScdYUdy', 1,  NULL, NULL, NULL, NULL, 'API', 'Service Desk',  '0000-00-00 00:00:00',  '0000-00-00 00:00:00');